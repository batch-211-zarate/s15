console.log("Hello World");

//Javascript renders web pages in an interactive and dynamic fashion. 
//Meaning, it enables us to create dynamicallu updating content, 
//control multimedia and animate images

//Syntax, Statements, and Comments

//Statements:
//Statements in programming are instructions that we tell the computer to perform
//JS statements usually end ith a semicolon (;)
//Semicolon are not required in JS, but we will use it to help us train to locate where a statement end


//Syntax
//Syntax in programming, it is the set of rules that describes how statements must be constructed.

//Comments 
//Comments are parts of the code that gets ignored by the languages. 
//Comments are meant to describe the written code.

/*

	There are twwo type of comments:
	1. The single-line comment (ctrl+/)
	** denoted by two slashes (//)

	2. The multi-line comment (ctrl + shift +/)
	**denoted by slash and asterisk

*/

//Variables

//It is used to contain data
// This make it easier for us to associate information stored in our devices rto actual "names" about information

//Declaring variables
//declaring variables - tells our devices that a variable name is created and is ready to store data


let myVariables;
console.log(myVariables);	// undefined

//Declaring a variable without giving it a value will autonmatically assign it with the value
//of "undefined", meaning the variable's value was "not defined".

//Syntax
	// let/const variableName;

//console.log() is useful for printing values of variables or certain results of code into Google Chrome browser's console
//constant use of this throughout developing an application will save us time and builds good habits
//in always checking for the output of the code.

let hello;
console.log(hello); // undefined

//Variables must be declared first before they are used.
//using variables before they are declared will return an error.


/*
	Guides in writing variables
		1. Use the 'let' keyword followed by our variable name of your choosing and use the assignment
		operator (=) to assign a value
		2. Variable names should start with a lowercase character, use camelCase for multiple words
		3. For constant variables, use the 'const'keyword
		4. Variables names should be indicative (or descriptive) of the value being stored to avoid confusion.

*/

//Declaring and Initialize variables
	//Initializing variables - the instance when a variable is given its initial or startin value
	//Syntax
		// let/const variableName = value;

		let productName = 'desktop computer';
		console.log(productName);

		let productPrice = 18999;
		console.log(productPrice);

		const interest = 3.539;
		// const pi = 3.1416;

		//Reassigning variables values
		//reassigning a variable means changing its initial or previous value into another value
		//Syntax
			//variableName = newValue;
		productName = 'Laptop';
		console.log(productName);

		console.log(interest);
		// console.log(pi);

		//let variables cannot be re-declared within its scope so this will work:
		// let friend = 'Kate';
		// friend = 'Nej';
		// console.log(friend);

		// let friend = 'Kate';
		// let friend = 'Nej'; //error
		// console.log(friend);
		//error: identifier 'friend' has already been declared

		// interest = 4.489; //error

//Reassigning variables vs. initializing variables
//declare a variable first
	
	let	supplier;
	//Initialization is done after the variable has been declared
	//This is considered as initialization because it is first time that a value has been assigned to a variable
	supplier = 'John Smith Tradings';
	console.log(supplier);

	//considered as Reassigning because its initial value was already declared
	supplier = 'Zuitt Store';
	console.log(supplier);


	//we cannot declare a constant variable without initialization
	// const pi;
	// pi = '3.1416';
	// console.log(pi); //error

	//var
	//var is also used in declaring a variable
	//let/const they were introduced as new feature in E26 (2015)

	// a = 5;
	// console.log(a); //output: 5
	// var a;

	//Hoisting is JS's default behavior of moving decalrations to the top
	//In terms of variables and constants, keyword var is hoisted and let and const does not allow hoisting

	// a = 5;
	// console.log(a);
	// let a;

//let/const local/global scope
	
	//Scope essentially means wherw these variables are available for use
	//let and const are block-scoped
	//block is a chunk of code bounded by {}. A block live in curly braces. Anything within curly braces is a block.
	//so variable declared in a block with let is only available for use within that block

	// let outerVariable = 'Hello';
	// {
	// 	let innerVariable = " Hello again";
	// }	

	// console.log(outerVariable);
	// console.log(innerVariable); //innerVariable is not defined

	//Multiple variable declarations
	//multiple variables may be declared in one line
	//though it is quicker to do without having to retype the 'let' keywords when declaring variables
	//using multiple keywords makes code easier to read and determine what kind of variable has been created

	// let productCode = 'DC017', productBrand = 'Dell';
	let productCode = 'DC017';
	const productBrand = 'Dell';
	console.log(productCode, productBrand); // DC017 Dell

	//Using a variable with a reserved keyword
	// const let = 'hello';
	// console.log(let); //error

	//Data Types

	//Strings
	//Strings are a series of characters that create a word, phrase, a sentence or anything related to creating text
	//Strings in JS can be written using either a single ('') or double ("") quote.

	let country = 'Philippines';
	let province = "Metro Manila";

	//Concatenating strings
	//Multiple strings values can be combined to create a single strings using the "+ symbol"

	let fullAddress = province + ', ' + country;
	console.log(fullAddress); //Metro Manila, Philippines

	let greeting = 'i live in the' + country;
	console.log(greeting); //I live in the Philippines

	//The escape characters (\) in strings in combination with other characters can produce different effects
	// "\n" refers to creating a new line in between text

	let mailAddress = 'Metro Manila \n\Philippines';
	console.log(mailAddress);

	//Using the double quotes along with single quotes can allow us to easily include single quotes in texts
	//without using the escape characters

	let message = "John's employees went home early";
	console.log(message);
	message = 'John\'s employees went home early';

	//Numbers

	//Integers / Whole Numbers

	let headCount = 26;
	console.log(headCount); //26

	//Decimal Numbers / Fractions
	let grade = 98.7;
	console.log(grade); //98.7

	//Exponential Notation 
	let planetDistance = 2e10;
	console.log(planetDistance); //20000000000

	//Combine text and strings
	console.log("John's grade last quarter is " + grade);
	//John's grade last quarter is 98.7

	//Boolean
	//Boolean values are normally used to store values relating to the states of certain things
	//This will be useful in further discussions about creating logic to make our application respond
	//to certain situations/scenarios

	let isMarried = false;
	let inGoodConduct = true;

	console.log("isMarried: " + isMarried);
	console.log("inGoodConduct: " + inGoodConduct);

	//Arrays
	//Arrays are special kind of data type that us used to store multiple values
	//Arrays can store different data types but is normamlly used to store similar data types

	//similar data types
	//Syntax
		//let/const arrayName = [elementA, elementB, elementC, ...]
			//Array Literals

			let grades = [98.7, 92.1, 90.2, 94.6];
			console.log(grades);

			//different data types
			//sorting differenr data tyoes insidee an array is NOT RECOMMENDED because it will not make sense
			//in contect of programming

			let details = ["John", "Smith", 32, true ];
			console.log(details);

			//Objects
			//Objects are another special kind of data type that's used to mimic real world objects items
			//They're used to create complex data that contains pieces of information that are relevant to each other
			//Every individual piece of information is called property of the object

			//Syntax
				//let/const objectName = {
					//propertyA : value,
					//propertyB : value
				//}

			let person = {
				fullName : 'Edwward Scissorhands',
				age: 25,
				isMarried: false,
				contact: ["+63912344556", "8123 4567"],
				address: {
					houseNumber: '345',
					city: 'Manila'
				}
			}

			console.log(person);

			let myGrades = {
				firstGrading: 98.7,
				secondGrading: 92.1,
				thirdGrading: 90.2,
				fourthGrading: 94.6
			}
			console.log(myGrades);

			//typeof operator

			console.log(typeof myGrades); //object
			console.log(typeof grades); //object


			const anime = ['OP', 'OPM', 'AOT', 'BNHA'];

			anime[0] = ['JJK'];
			console.log(anime);

			//Null
			//Null simply means no VALUE
			let spouse = null;
			console.log(spouse);

			let myNumber = 0; //0
			let myString = ''; //
			console.log(myNumber);
			console.log(myString);

			//Undefined
			//represents the state of a variable that has been declared but w/ 0 an assigned value

			let fullName;
			console.log(fullName); //undefined

			//one clear difference between undefined and null is that for undefined, a variable was created but
			//was not provided a value.








